﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.LSP
{
    public class ClientCodeLSPRefactored
    {
        public void ClientCode()
        {
            List<IHunt> hunterAnimals = new List<IHunt>();
            List<IWalk> walkerAnimals = new List<IWalk>();

            hunterAnimals.Add(new TigerRefactored());
            hunterAnimals.Add(new WolfRefactored());
            walkerAnimals.Add(new TurtleRefactored());

            GoToHunt(hunterAnimals);
            GoToWalk(walkerAnimals);
        }

        public void GoToHunt(List<IHunt> hunters)
        {
            foreach (var hunter in hunters)
            {
                hunter.Hunt();
            }
        }

        public void GoToWalk(List<IWalk> walkers)
        {
            foreach (var walker in walkers)
            {
                walker.Walk();
            }
        }
    }
}
