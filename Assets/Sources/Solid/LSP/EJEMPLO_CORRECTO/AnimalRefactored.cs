﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.LSP
{
    public class TigerRefactored : IHunt, IWalk, IRun
    {
        public void Hunt()
        {
        }
        public void Run()
        {
        }
        public void Walk()
        {
        }
    }
    public class WolfRefactored : IHunt, IWalk, IRun
    {
        public void Hunt()
        {
        }
        public void Run()
        {
        }
        public void Walk()
        {
        }
    }
    public class TurtleRefactored : IWalk
    {
        public void Walk()
        {
        }
    }
}
