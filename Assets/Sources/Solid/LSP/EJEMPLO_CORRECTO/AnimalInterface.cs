﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.LSP
{
    public interface IRun
    {
        public void Run();
    }
    public interface IHunt
    {
        public void Hunt();
    }
    public interface IWalk
    {
        public void Walk();
    }
}
