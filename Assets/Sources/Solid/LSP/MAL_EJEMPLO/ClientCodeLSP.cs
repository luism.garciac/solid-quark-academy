﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.LSP
{
    public class ClientCodeLSP
    {
        public void ClientCode()
        {
            Animal tiger = new Tiger();
            Animal wolf = new Wolf();
            Animal turtle = new Turtle();

            tiger.Hunt();
            wolf.Run();
            turtle.Hunt();
        }
    }
}
