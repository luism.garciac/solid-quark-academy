﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace SOLID.SRP
{
        public class NotificationService
        {
            private readonly SmtpClient _smtpClient;

            public NotificationService(SmtpClient smtpClient)
            {
                _smtpClient = smtpClient;
            }

            public async Task SendEmail(MailMessage message)
            {
                await _smtpClient.SendMailAsync(message);
            }
        }
}
