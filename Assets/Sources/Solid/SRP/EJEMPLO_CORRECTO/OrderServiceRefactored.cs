﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace SOLID.SRP
{
    public class OrderServiceRefactored
    {
        private readonly NotificationService _notificationService;

        public OrderServiceRefactored(NotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        public async Task Add(Order order)
        {
            // 01. Código para crear la orden

            // 02. Notificar al cliente
            var message = new MailMessage("sales@admin.com", order.ClientEmail)
            {
                Subject = "Se le asignó una compra",
                Body = "Estimado,\n Hemos creado su nueva orden de compra .."
            };

            await this._notificationService.SendEmail(message);
        }
    }
}
