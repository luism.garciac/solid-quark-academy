﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SOLID.OCP
{
    public class OCPControllerBad
    {
        public async Task SendNotification()
        {
            var email = new Notification
            {
                NotificationType = "email",
                Email = "customer@email.com",
                Subject = "El asunto del correo"
            };

            var sms = new Notification
            {
                NotificationType = "sms",
                PhoneNumber = "+051199999999",
                Subject = "El mensaje del texto"
            };

            var notificationService = new NotificationService();
            await notificationService.Send(new List<Notification> { email, sms });
        }
    }
}
