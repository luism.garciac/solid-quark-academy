﻿namespace SOLID.OCP
{
    public class Notification
    {
        public string Email { get; internal set; }
        public string NotificationText { get; internal set; }
        public string NotificationType { get; internal set; }
        public string PhoneNumber { get; internal set; }
        public string Subject { get; internal set; }
    }
}