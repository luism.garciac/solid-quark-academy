using System.Collections.Generic;
using System.Threading.Tasks;

namespace SOLID.OCP
{
    public class NotificationService
    {
        public async Task Send(List<Notification> notifications)
        {
            foreach (var notification in notifications)
            {
                if (notification.NotificationType.Equals("sms"))
                {
                    await SendbySMS(notification.PhoneNumber, notification.Subject);
                }

                if (notification.NotificationType.Equals("email"))
                {
                    await SendbyEmail(notification.Email, notification.Subject);
                }
            }
        }

        private async Task SendbySMS(string phoneNumber, string subject)
        {
            // Logica para mandar el SMS
        }

        private async Task SendbyEmail(string to, string subject)
        {
            // Logica para mandar el email
        }
    }
}

