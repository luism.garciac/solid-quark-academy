using System.Threading.Tasks;

namespace SOLID.OCP
{
    public interface INotification
    {
        Task Notify();
    }
}
