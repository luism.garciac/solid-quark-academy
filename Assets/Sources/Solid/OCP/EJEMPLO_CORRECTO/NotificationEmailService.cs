﻿using System.Threading.Tasks;

namespace SOLID.OCP
{
    class NotificationEmailService : INotification
    {
        private readonly string _to;
        private readonly string _subject;

        public NotificationEmailService(string to, string subject)
        {
            _to = to;
            _subject = subject;
        }

        public Task Notify()
        {
            throw new System.NotImplementedException();
        }
    }
}
