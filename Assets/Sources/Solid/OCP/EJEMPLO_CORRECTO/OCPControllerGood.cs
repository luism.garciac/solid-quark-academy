﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.OCP
{
    class OCPControllerGood
    {
        public async Task SendNotification()
        {
            var notificationsx = new List<INotification>
                {
                new NotificationEmailService("customer@email.com", "El motivo del correo"),
                new NotificationSmsService("+05199999", "El asunto del mensaje de texto"),
                // .. podemos implementar más notificaciones
                };

            var notificationService = new NotificationServiceRefactored();
            await notificationService.Send(notificationsx);
        }
    }
}
