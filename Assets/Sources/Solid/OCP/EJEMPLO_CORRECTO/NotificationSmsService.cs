﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.OCP
{
    class NotificationSmsService : INotification
    {
        private readonly string _phoneNumber;
        private readonly string _subject;

        public NotificationSmsService(string phoneNumber, string subject)
        {
            _phoneNumber = phoneNumber;
            _subject = subject;
        }

        public Task Notify()
        {
            throw new NotImplementedException();
        }
    }
}
