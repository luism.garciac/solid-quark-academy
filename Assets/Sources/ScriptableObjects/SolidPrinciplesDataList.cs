using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SolidDataList", menuName = "SolidDataList", order = 1)]
public class SolidPrinciplesDataList : ScriptableObject
{
    public List<SolidPrincipleIndividual>  _solidPrinciples;

    [System.Serializable]
    public class SolidPrincipleIndividual
    {
        public string _title;

        [TextArea(10, 100)]
        public string _description;

        public Sprite _badExample;
        public Sprite _goodExample;
    }

}
