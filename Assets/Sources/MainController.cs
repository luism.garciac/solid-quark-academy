using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainController : MonoBehaviour
{
    #region InitialSetups

    [SerializeField] private SolidPrinciplesDataList _solidPrinciplesData;

    [SerializeField] private GameObject _principleDescriptionGO;
    [SerializeField] private GameObject _badExampleGO;
    [SerializeField] private GameObject _goodExampleGO;

    [SerializeField] private TMP_Text _principleTitle;
    [SerializeField] private TMP_Text _principleDescription;
    [SerializeField] private Image _badExampleImage;
    [SerializeField] private Image _goodExampleImage;

    private void Start()
    {
        SetupPrinciple("Bienvenido");
    }

    #endregion

    #region ApplicationMethods

    public void SetupPrinciple(string principleName)
    {
        foreach (var principle in _solidPrinciplesData._solidPrinciples)
        {
            if (principle._title == principleName)
            {
                _principleTitle.text = principle._title;
                _principleDescription.text = principle._description;
                _badExampleImage.sprite = principle._badExample;
                _goodExampleImage.sprite = principle._goodExample;
            }
        }

        _principleDescriptionGO.SetActive(true);
        _goodExampleGO.SetActive(false);
        _badExampleGO.SetActive(false);
    }

    public void SwitchSection(GameObject section)
    {
        DisableAllSections();
        section.SetActive(true);
    }

    public void DisableAllSections()
    {
        _principleDescriptionGO.SetActive(false);
        _goodExampleGO.SetActive(false);
        _badExampleGO.SetActive(false);
    }

    public void OnCloseAppButtonClick()
    {
        Application.Quit();
    }

    #endregion

}
